import os
import subprocess
from fractions import Fraction
import json
from pathlib import Path

grammar_input_path = Path("test_grammars/vert_grammar2.txt")
script_path = Path("../qparselib/build/equiv1")
timeline_path= "timelines/temp_tim1.txt"


def compute_grammar(score, measure_quarter_lenght= None,delete_rests = False):  
    #measure quarter_length is used to check if the metric is correct
    
    failure_counter = 0  # to keep track of the number of failure
    wrong_sum_counter = 0 #to keep track of the number of bars that do not sum to the correct value
    total_measures = 0 #to keep track of the complete number of measures
    flatMeasuresDurations = []
    # save the duration tree for each bar
    for part_index, part in enumerate(score.parts):  # loop through parts
        for measure_index, measure_with_rests in enumerate(part.getElementsByClass('Measure')):
            total_measures += 1
            if len(measure_with_rests.voices) == 0:  # there is a single Voice ( == for the library there are no voices)
                if delete_rests:
                    measure = delete_rests(measure_with_rests)
                else: 
                     measure=measure_with_rests
                note_list = list(measure.getElementsByClass('GeneralNote'))
                if measure_quarter_lenght is None: #append everything
                    flatMeasuresDurations.append(note_list)
                else: #append only the measure that sum to the correct value
                        if sum([n.duration.quarterLength for n in note_list]) == measure_quarter_lenght:
                            flatMeasuresDurations.append(note_list)
                        else:
                            wrong_sum_counter +=1
            else:  # there are multiple voices (or an array with just one voice)
                for voice_with_rest in measure_with_rests.voices:
                    if delete_rests:
                        voice = delete_rests(voice_with_rest)
                    else:
                        voice = voice_with_rest
                    note_list = list(voice.getElementsByClass('GeneralNote'))
                    if measure_quarter_lenght is None: #append everything
                        flatMeasuresDurations.append(note_list)
                    else: #append only the measure that sum to the correct value
                        if sum([n.duration.quarterLength for n in note_list]) == measure_quarter_lenght:
                            flatMeasuresDurations.append(note_list)
                        else:
                            wrong_sum_counter +=1
                                
                            

    # process and sum over all the bars
    sum_rule = {}  

    for dur in flatMeasuresDurations:
        try:
            c_input = save_timeline(dur, timeline_path)
            unix_command = "{0} -v 0 -i {1} -a {2} -penalty -inference".format(script_path,timeline_path, grammar_input_path)
            # print(unix_command)
            if os.name == 'nt': #on windows, we call the ubuntu bash
                c_output = subprocess.check_output(["bash", "-c", unix_command])
            else:
                c_output = subprocess.check_output(unix_command, shell=True)
            # print(c_output)
            rule_list = json.loads(c_output.decode("utf-8").splitlines()[8]) #some useless stuff in the first 7 rows
            print(rule_list)
            sum_rule = { k: sum_rule.get(k, 0) + rule_list.get(k, 0) for k in set(sum_rule) | set(rule_list) } #"sum" the two dictionaries
        except:
            failure_counter += 1
    
    return sum_rule, failure_counter, wrong_sum_counter, total_measures



def save_timeline(notes, path):
    durs = [Fraction(note.duration.quarterLength) for note in notes]
    normalized_dur = [Fraction(dur/sum(durs)) for dur in durs]
    dur_string = ""
    for n_dur in normalized_dur:
        dur_string = "{0}{1}\n".format(dur_string, str(n_dur.numerator)+ "/"+str(n_dur.denominator))
    dur_string = dur_string + "\n"
    dur_string = dur_string[0:-1] #delete last comma
    with open(Path(path), "w") as f:
        f.write(dur_string)
    return dur_string
